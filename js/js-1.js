let fruitMap = new Map([
    ['ананасов',   '500 гр'],
    ['яблок', '350 гр'],
    ['груш',   '400 гр'],
    ['слив',   '300 гр']
]);

console.log(fruitMap);

for(let entry of fruitMap) {
    console.log(entry);
};

fruitMap.forEach( (value, key, map) => {
    console.log(`${key}: ${value}`);
});






